program main

  use driver
  use coulomb
  implicit none

  real(8) :: x,y,z,length,width,depth,dip,strike,east,north,disl(3),nu,mu,     &
     u(3),uu(9),rcv_strike,rcv_dip,rcv_rake,fric,sxx,syy,szz,sxy,sxz,syz,sh,   &
     no,cff
  integer :: i,j

  ! Obs depth and material properties
  nu=0.25
  mu=3.0E+10
  z =0.0

  ! Reciever fault parameters
  rcv_strike=0.0
  rcv_dip   =90.0
  rcv_rake  =180.0
  fric      =0.4

  ! Slipping fault parameters
  length=20000.0
  width =10000.0
  depth =0.0
  dip   =90.0
  strike=0.0
  east  =0.0
  north =0.0
  disl  =(/10.0,0.0,0.0/)

  uu=0.0
  do i=-25000,25000,500
     do j=-25000,25000,500
        x=real(i)
        y=real(j)
        call okada3d(length,width,depth,dip,strike,east,north,disl,x,y,z,nu,   &
           mu,u,uu)
        call e2s(uu,nu,mu,sxx,syy,szz,sxy,sxz,syz)
        call calc_coulomb(rcv_strike,rcv_dip,rcv_rake,fric,sxx,syy,szz,sxy,    &
           sxz,syz,sh,no,cff)
        print'(5(F0.3,1X))', x/1000.0,y/1000.0,sh,no,cff
     end do
  end do

contains

  subroutine e2s(uu,nu,mu,sxx,syy,szz,sxy,sxz,syz)
    implicit none
    real(8) :: uu(9),nu,mu,sxx,syy,szz,sxy,sxz,syz,lambda,vol
    lambda=2.0*mu*nu/(1.0-2.0*nu)
    vol=uu(1)+uu(5)+uu(9)
    sxx=lambda*vol+2*mu*uu(1)
    syy=lambda*vol+2*mu*uu(5)
    szz=lambda*vol+2*mu*uu(9)
    sxy=mu*(uu(2)+uu(4))
    sxz=mu*(uu(3)+uu(7))
    syz=mu*(uu(6)+uu(8))
  end subroutine e2s

end program main
