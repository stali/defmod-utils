module coulomb

  ! - Calculates Coulomb stress on a fault plane
  ! - Strike, Dip and Rake are in Aki and Richards convention
  ! - +X is east and +Y is north
  ! - Use at your own risk
  !
  ! <tabrezali@gmail.com>

contains

  subroutine calc_coulomb(strike,dip,rake,fric,sxx,syy,szz,sxy,sxz,syz,sh,no,  &
     cff)
    implicit none
    real(8) :: pi,rad,phi,strike,dip,rake,fric,sdip,cdip,srake,crake,nhat(3),  &
       vhat(3),that(3),Sij(3,3),snn,snv,svv,stt,snt,cff,sh,no,sxx,syy,sxy,sxz, &
       syz, szz
    pi=3.14159265359d0
    rad=180.0d0/pi
    phi=strike/rad
    sdip=sin(dip/rad)
    cdip=cos(dip/rad)
    srake=sin(rake/rad)
    crake=cos(rake/rad)
    nhat=(/-sdip*(-cos(phi)),-sdip*sin(phi), cdip/)
    vhat=(/-cdip*(-cos(phi)),-cdip*sin(phi),-sdip/)
    call axb(nhat,vhat,that)
    Sij=reshape((/sxx,sxy,sxz,sxy,syy,syz,sxz,syz,szz/),shape=(/3,3/))
    snn=tprod(nhat,Sij,nhat)
    snv=tprod(nhat,Sij,vhat)
    svv=tprod(vhat,Sij,vhat)
    stt=tprod(that,Sij,that)
    snt=tprod(nhat,Sij,that)
    sh=snt*crake-snv*srake
    no=snn
    cff=sh+fric*no
  end subroutine calc_coulomb

  function tprod(n1,tensor,n2)
    implicit none
    real(8) :: tprod,n1(3),tensor(3,3),n2(3)
    integer :: i, j
    tprod=0.0d0
    do i=1,3
       do j=1,3
          tprod=tprod+n1(i)*tensor(i,j)*n2(j)
       end do
    end do
  end function tprod

  subroutine axb(a,b,c)
    implicit none
    real(8) :: a(3),b(3),c(3)
    c(1)=a(2)*b(3)-b(2)*a(3)
    c(2)=b(1)*a(3)-a(1)*b(3)
    c(3)=a(1)*b(2)-b(1)*a(2)
  end subroutine axb

end module coulomb
