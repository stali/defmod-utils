program main

  use driver
  implicit none

  real(8) :: x,y,z,length,width,depth,dip,strike,east,north,disl(3),nu,mu,     &
     u(3),uu(9)
  integer :: i, j

  ! Obs depth and material properties
  nu=0.25
  mu=3.0E+10
  z =0.0 ! Between 0 and -Inf

  ! Fault parameters
  length=20000.0
  width =10000.0
  depth =0.0  ! Between 0 and -Inf
  dip   =90.0 ! Between 0 and 90
  strike=0.0  ! Between 0 and 360
  east  =0.0
  north =0.0
  disl  =(/10.0,0.0,0.0/) ! SS, DS, TS

  uu=0.0
  do i=-25000,25000,2500
     do j=-25000,25000,2500
        x=real(i)
        y=real(j)
        call okada3d(length,width,depth,dip,strike,east,north,disl,x,y,z,nu,   &
           mu,u,uu)
        print'(2(F0.3,1X),3(F0.6,1X))', x/1000.0,y/1000.0,u
     end do
  end do

end program main
