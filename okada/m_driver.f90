module driver

  ! - These are drivers to Okada's routines (see m_okada.f90)
  ! - Use at your own risk
  !
  ! <tabrezali@gmail.com>

  use okada

  real(8), parameter :: deg2rad=0.01745329252d0,pi2inv=0.15915494309d0,        &
     pi=3.14159265359d0

contains

  subroutine okada3d(length,width,depth,dip,strike,east,north,disl,x,y,z,nu,   &
     mu,u,uu)
    implicit none
    real(8) :: length,width,depth,dip,strike,east,north,disl(3),x,y,z,nu,mu,   &
       u(3),uu(9),cdepth,cdip,lambda,alp,angle,cd,sd,cs,ss,xl,yl,zl,depthl,    &
       al1,al2,aw1,aw2,u1,u2,uxx,uyx,uzx,uxy,uyy,uzy,uxz,uyz
    integer :: iret
    cdepth=-1.0d0*depth
    cdip=dip-180.0d0
    lambda=2.0d0*mu*nu/(1-2.0d0*nu)
    alp=(lambda+mu)/(lambda+2.0d0*mu)
    angle=(strike-90.0d0)*deg2rad
    cd=cos(cdip*deg2rad)
    sd=sin(cdip*deg2rad)
    cs=cos(angle)
    ss=sin(angle)
    xl=cs*(x-east)-ss*(y-north)
    yl=ss*(x-east)+cs*(y-north)-0.5d0*cd*width
    zl=z
    depthl=cdepth-0.5d0*width*sd
    al1=length/2.0d0
    al2=al1
    aw1=width/2.0d0
    aw2=aw1
    call dc3d(alp,xl,yl,zl,depthl,cdip,al1,al2,aw1,aw2,disl(1),disl(2),        &
       disl(3),u(1),u(2),u(3),uu(1),uu(2),uu(3),uu(4),uu(5),uu(6),uu(7),uu(8), &
       uu(9),iret)
    u1=u(1)
    u2=u(2)
    uxx=uu(1)
    uyx=uu(2)
    uzx=uu(3)
    uxy=uu(4)
    uyy=uu(5)
    uzy=uu(6)
    uxz=uu(7)
    uyz=uu(8)
    u(1)=cs*u1+ss*u2
    u(2)=-ss*u1+cs*u2
    uu(1)=cs*cs*uxx+cs*ss*(uxy+uyx)+ss*ss*uyy
    uu(4)=cs*cs*uxy-ss*ss*uyx+cs*ss*(-uxx+uyy)
    uu(7)=cs*uxz+ss*uyz
    uu(2)=-(ss*(cs*uxx+ss*uxy))+cs*(cs*uyx+ss*uyy)
    uu(5)=ss*ss*uxx-cs*ss*(uxy+uyx)+cs*cs*uyy
    uu(8)=-(ss*uxz)+cs*uyz
    uu(3)=cs*uzx+ss*uzy
    uu(6)=-(ss*uzx)+cs*uzy
  end subroutine okada3d

end module driver
