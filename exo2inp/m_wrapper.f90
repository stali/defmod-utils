! <tabrezali@gmail.com>

module wrapper

  use netcdf
  implicit none
  integer :: ncid, varid, varsize

  interface get_array
     module procedure get_array_1d_int, get_array_1d_real
     module procedure get_array_2d_int, get_array_2d_real
  end interface

contains

  function i2c(i) result(c)
    implicit none
    integer :: i
    character(12) :: c
    write(c,'(I0)') i
    c=trim(adjustl(c))
  end function i2c

  subroutine check(status)
    implicit none
    integer :: status
    if (status/=nf90_noerr) then
       print *, trim(nf90_strerror(status))
       stop
    end if
  end subroutine check

  subroutine get_array_1d_int(str, array)
    implicit none
    character(*) :: str
    integer :: array(:)
    call check(nf90_inq_varid(ncid, str, varid))
    call check(nf90_get_var(ncid, varid, array))
  end subroutine get_array_1d_int

  subroutine get_array_1d_real(str, array)
    implicit none
    character(*) :: str
    real :: array(:)
    call check(nf90_inq_varid(ncid, str, varid))
    call check(nf90_get_var(ncid, varid, array))
  end subroutine get_array_1d_real

  subroutine get_array_2d_int(str, array)
    implicit none
    character(*) :: str
    integer :: array(:, :)
    call check(nf90_inq_varid(ncid, str, varid))
    call check(nf90_get_var(ncid, varid, array))
  end subroutine get_array_2d_int

  subroutine get_array_2d_real(str, array)
    implicit none
    character(*) :: str
    real :: array(:, :)
    call check(nf90_inq_varid(ncid, str, varid))
    call check(nf90_get_var(ncid, varid, array))
  end subroutine get_array_2d_real

  subroutine get_varsize(str, varsize)
    implicit none
    character(*) :: str
    integer :: varsize
    call check(nf90_inq_dimid(ncid, str, varid))
    call check(nf90_inquire_dimension(ncid, varid, len=varsize))
  end subroutine get_varsize

end module wrapper
