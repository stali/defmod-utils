! Utility to create a Defmod input file from an EXODUS II file.
!
! NOTE: Only simple fault loading scenarios such as uniform slip, uniform
! rupture velocity, slip-rate, etc. are supported. Modify/Extend as needed.
!
! <tabrezali@gmail.com>

program main

  use wrapper
  implicit none
  character(3) :: etype
  character(256) :: efile, ifile, fmt, stype, msg
  integer :: nnds, nels, nmts, nces, nfrcs, ntrcs, nabcs, dmn, n, i, j, k,     &
     l, p, fp, ctr
  integer, allocatable :: bc(:,:), nodes(:,:), work(:,:), xbc(:)
  real :: t1, t2, dist, t0, rv, rt
  real, allocatable :: coords(:,:), x(:), x0(:), val(:), cea(:,:)
  type n_set
     integer, pointer :: size(:)
  end type n_set
  type(n_set), allocatable :: cubit_nset(:)

  print*, "Enter the EXODUS II filename"
  read*, efile
  call check(nf90_open(efile, nf90_nowrite, ncid))
  ifile=trim(efile)
  open(20, file=trim(ifile)//".inp", status="replace")

  ! SIMULATION PARAMETERS
  print*, "Enter the element name (tri/quad/tet/hex)"
  read*, etype
  print*, "Enter the problem type (explicit or implicit/implicit-p/-v/-pv)"
  read*, stype
  if (etype=="tri") j=12  ! 'j' here is a relatively safe overestimate
  if (etype=="qua") j=15  !   - increase if assembly takes more than a min
  if (etype=="tet") j=36  !   - decrease to optimize on-rank memory use
  if (etype=="hex") j=51
  write(20,'(2(A,1X),I0)') trim(stype), trim(etype), j

  call get_varsize("num_elem", nels)
  call get_varsize("num_nodes", nnds)
  call get_varsize("num_el_blk", nmts)
  write(20,'(7(I0,1X))') nels, nnds, nmts, 0, 0, 0, 0

  print*, "Enter total-t, dt, output-freq, dx-flag (E.g., 100.0 0.1 250 0)"
  read*, t1, t2, i, j
  write(20,'(2(ES9.3,1X),2(I0,1X))') t1, t2, i, j

  if (etype=="tri" .or. etype=="qua") dmn=2
  if (etype=="tet" .or. etype=="hex") dmn=3
  p=0; if (stype=="implicit-p" .or. stype=="implicit-pv") p=1

  ! DAMPING COEFFICIENTS
  if (stype=="explicit") write(20,'(2(ES9.3,1X))') 0.0, 0.5/100.0

  ! ELEMENT DATA
  if (etype=="tri") k=3; if (etype=="qua") k=4
  if (etype=="tet") k=4; if (etype=="hex") k=8
  ctr=1
  do i=1, nmts
     call get_varsize("num_el_in_blk"//i2c(i), varsize)
     allocate(nodes(k, varsize))
     call get_array("connect"//i2c(i), nodes)
     do j=1, varsize
        write(20,'(9(I0,1X))') nodes(:,j), ctr
     end do
     deallocate(nodes)
     ctr=ctr+1
  end do

  ! NODE DATA WITH FIXED/FREE BC FLAG(S)
  allocate(bc(nnds,dmn+p), xbc(dmn+p)); bc=1; xbc=1
  j=nf90_inq_dimid(ncid, "num_node_sets", varid)
  if (j/=0) then
     n=0
  else
     call get_varsize("num_node_sets", n)
  end if
  ctr=0
  do i=1, n
     call get_varsize("num_nod_ns"//i2c(i), varsize)
     ctr=ctr+varsize
  end do
  allocate(work(1+dmn+p,ctr), cubit_nset(n)); work=0
  print*, "Enter number of node-sets for which fixed (i.e., zero)/free BCs"
  print*, "      are being specified"
  print*, "      NOTE: Non-zero disp./pressure BCs will be specified later"
  read*, n
  ctr=0
  do i=1, n
     print*, "Enter a node-set #"; read*, j
     call get_varsize("num_nod_ns"//i2c(j), varsize)
     allocate(cubit_nset(i)%size(varsize))
     call get_array("node_ns"//i2c(j), cubit_nset(i)%size)
     print*, "Enter x,y,(z),(p) BC code for the node-set (0:fixed, 1:free,"
     print*, "     -1: Winkler foundation)"
     read*, xbc
     do j=1, varsize
        ctr=ctr+1
        work(1,ctr)=cubit_nset(i)%size(j); work(2:,ctr)=xbc
     end do
     deallocate(cubit_nset(i)%size)
  end do
  do i=1,ctr
     bc(work(1,i),:)=work(2:,i)
  end do
  deallocate(work, cubit_nset)
  allocate(coords(nnds,dmn))
  j=nf90_inq_varid(ncid, "coord", varid)
  if (j==0) then
     call get_array("coord" , coords) ! x(:) y(:) z(:)
  else
     call get_array("coordx", coords(:,1))
     call get_array("coordy", coords(:,2))
     if (dmn==3) call get_array("coordz", coords(:,3))
  end if
  if (dmn==2) fmt="(2(F0.3,1X),3(I0,1X))"
  if (dmn==3) fmt="(3(F0.3,1X),4(I0,1X))"
  do i=1, nnds
     write(20,fmt) coords(i,:), bc(i,:)
  end do
  deallocate(bc, xbc)

  ! MATERIAL DATA
  if (stype=="implicit" .or. stype=="explicit") then
     j=3; msg="      7.5E10 0.25 3000.0"
  else if (stype=="implicit-v") then
     j=5; msg="      7.5E10 0.25 3000.0 1.0E25 1.0"
  else ! i.e., for all other cases
     j=9; msg="      7.5E10 0.25 3000.0 1.0E25 1.0 1.0E-9 1.0 0.05 2.2E9"
  end if
  allocate(val(j))
  fmt="("//trim(i2c(j))//"(ES9.3,1X))"
  do i=1, nmts
     print'(A,I0,A)', " Enter material properties for blk # ", i, ", e.g.,"
     print*, trim(msg)
     read*, val
     write(20,fmt) val
  end do
  deallocate(val)

  ! CONSTRAINT EQUATIONS

  ! 1. FOR NON-ZERO DISP./PRESSURE BCs
  print*, "Enter number of node-sets for which non-zero disp./pressure BCs"
  print*, "      are being specified"
  read*, n
  fmt="("//trim(i2c(dmn+p))//"(ES10.3,1X),I0)"
  allocate(cea(1,dmn+p),val(dmn+p))
  ctr=0
  do i=1,n
     cea=0
     print*, "Enter a node-set #"; read*, j
     call get_varsize("num_nod_ns"//i2c(j), varsize)
     allocate(work(varsize,1))
     call get_array("node_ns"//i2c(j), work(:,1))
     if (dmn==2) print*, "Enter dof number Ux=1, Uy=2, (P=3)"
     if (dmn==3) print*, "Enter dof number Ux=1, Uy=2, Uz=3, (P=4)"
     read*, j; cea(1,j)=1
     print*, "Enter a value for the dof"
     read*, val(1)
     print*, "Enter start and end time for the constraint (Enter '0.0 0.0' to"
     print*, "      keep the above value constant throughout the simulation)"
     read*, t1, t2
     do j=1, varsize
        write(20,'(I0)') 1
        write(20,fmt) cea(1,:), work(j,1)
        write(20,'(3(ES10.3,1X))') val(1), t1, t2
        ctr=ctr+1
     end do
     deallocate(work)
  end do
  deallocate(cea)

  ! 2. FOR FAULT SLIP
  print*, "Enter number of faults in the model"
  read*, n
  do l=1,n
     val=0
     print*, "Enter the node-set # for the foot wall"; read*, j
     call get_varsize("num_nod_ns"//i2c(j), n)
     allocate(work(2,n), x(dmn), x0(dmn))
     call get_array("node_ns"//i2c(j), work(1,:))
     print*, "Enter the node-set # for the hanging wall"; read*, j
     call get_array("node_ns"//i2c(j), work(2,:))
     if (stype=="explicit") then
        print*, "Enter hypocentral coordinates (x,y,(z))"
        read*, x0
     end if
     print*, "Enter slip along with its direction (x,y,(z))"
     read*, val(1:dmn)
     fp=0
     if(p==1) then
        print*, "Is the fault permeable (0:no, 1: yes)"
        read*, fp
     end if
     allocate(cea(dmn+fp,dmn+p)); cea=0
     do i=1,dmn+fp
        cea(i,i)=1
     end do
     if (stype/="explicit") then
        print*, "Enter start and end time for the slip"
        read*, t1, t2
     end if
     if (stype=="explicit") then
        print*, "Enter rupture initialization time, it's velocity and rise"
        print*, "      time for the slip, e.g., 0.0 3162.3 1.0"
        read*, t0, rv, rt
     end if
     do i=1, n
        do j=1, n
           if (work(1,i)/=work(2,j) .and.                                      &
              all(abs(coords(work(1,i),:)-coords(work(2,j),:))<0.001)) then
              if (stype=="explicit") then
                 x=coords(work(1,i),:)
                 dist=sqrt(sum((x-x0)**2)) ! Propagating front is circular
                 t1=t0+(1000.0*dist/rv); t2=t1+rt
              end if
              do k=1, dmn+fp
                 write(20,'(I0)') 2
                 write(20,fmt)  cea(k,:), work(1,i)
                 write(20,fmt) -cea(k,:), work(2,j)
                 write(20,'(3(ES10.3,1X))') val(k), t1, t2
                 ctr=ctr+1
              end do
           end if
        end do
     end do
     deallocate(work, x, x0, cea)
  end do
  nces=ctr
  deallocate(coords)

  ! FORCE/FLOW BCs
  print*, "Enter number of node-sets for which non-zero force/flow BCs are"
  print*, "      being specified"
  read*, n
  fmt="(1(I0,1X),"//trim(i2c(dmn+p))//"(ES10.3,1X),2(ES9.3,1X))"
  nfrcs=0
  do i=1,n
     print*, "Enter a node-set #"; read*, j
     call get_varsize("num_nod_ns"//i2c(j), varsize)
     allocate(work(varsize,1))
     call get_array("node_ns"//i2c(j), work(:,1))
     print*, "Enter x,y,(z),(p) forcing(s)"
     read*, val
     print*, "Enter start and end time for the forcing"
     read*, t1, t2
     do j=1, varsize
        write(20,fmt) work(j,1), val, t1, t2
        nfrcs=nfrcs+1
     end do
     deallocate(work)
  end do

  ! TRACTION/FLUX BCs
  ntrcs=0
  print*, "Enter total number of side sets with traction/flux BCs"
  read*, n
  fmt="(2(I0,1X),"//trim(i2c(dmn+p))//"(ES10.3,1X),2(ES9.3,1X))"
  do i=1,n
     print*, "Enter a side-set #"; read*, j
     call get_varsize("num_side_ss"//i2c(j), varsize)
     allocate(work(varsize,2))
     call get_array("elem_ss"//i2c(j), work(:,1))
     call get_array("side_ss"//i2c(j), work(:,2))
     print*, "Enter x,y,(z),(p) tractions/flux"
     read*, val
     print*, "Enter start and end time for the loading"
     read*, t1, t2
     do k=1, varsize
        write(20,fmt) work(k,:), val, t1, t2
        ntrcs=ntrcs+1
     end do
     deallocate(work)
  end do
  deallocate(val)

  ! ABSORBING BCs
  nabcs=0
  if (stype=="explicit") then
     print*, "Enter total number of side sets with absorbing BCs"
     read*, n
     do i=1, n
        print*, "Enter a side-set #"; read*, j
        call get_varsize("num_side_ss"//i2c(j), varsize)
        allocate(work(varsize,2))
        call get_array("elem_ss"//i2c(j), work(:,1))
        call get_array("side_ss"//i2c(j), work(:,2))
        print*, "Enter 1/2/3 for -/+X/Y/Z direction"
        read*, j
        do k=1, varsize
           write(20,'(3(I0,1X))') work(k,:), j
           nabcs=nabcs+1
        end do
        deallocate(work)
     end do
  end if

  close(20)
  call check(nf90_close(ncid))

  ! SPIT PATCH TO FIX LINE #2
  open(30, file="ln2.diff", status="replace")
  write(30,'(3A)') "--- ", trim(efile), ".inp"
  write(30,'(3A)') "+++ ", trim(efile), ".inp"
  write(30,'(A)') "@@ -2 +2 @@"
  write(30,'(A,7(I0,1X))')"-", nels, nnds, nmts, 0, 0, 0, 0
  write(30,'(A,7(I0,1X))')"+", nels, nnds, nmts, nces, nfrcs, ntrcs, nabcs
  close(30)
  print*, ">>>>> Now execute command 'patch -i ln2.diff' to update line #2"

end program main
