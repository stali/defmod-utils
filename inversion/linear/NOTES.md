If  geometry, especially strike, dip and depth, are unknown then assume that the
fault can be parameterized by  a  single rectangular patch with uniform slip and
perform non-linear inversion (using simulated annealing) to estimate parameters.
Use focal mechanism, geologic information, scaling laws, aftershock distribution
etc. for initial estimate(s) and their bounds.

With geometrical parameters known  from  above (or otherwise), estimate the slip
distribution  by sub-dividing the fault into rectangular patches and solving the
linear inverse problem *Gm=d*. 

The *G* matrix is of size `(n_obs x obs_dof) x (n_patches x slip_dof)`, i.e.,

```
| U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |       | U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |
| U_y              |S1_P1   | U_y              |S1_P1  | U_y              |S1_P2   | U_y              |S1P2   ...

| U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |       | U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |
| U_y              |S2_P1   | U_y              |S2_P1  | U_y              |S2_P2   | U_y              |S2P2   ...

| U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |       | U_x [Okd(x_1.0)] |        | U_x [Okd(y_1.0)] |
| U_y              |S3_P1   | U_y              |S3_P1  | U_y              |S3_P2   | U_y              |S3P2   ...

...
```

where,

`n_obs` - number of observation points

`obs_dof` - components of observation data (e.g., Ux, Uy)

`n_patches` - number of patches used in the inversion

`slip_dof` - components of slip (strike/dip-slip) for each patch

`Okd` - Okada's function/subroutine

`U_x` - Okada's solution (x component)

`U_y` - Okada's solution (y component)

`x_1.0` - unit slip in x-dir

`y_1.0` - unit slip in y-dir

`Si` - ith station (use coordinates of this station in Okd)

`Pi` - ith patch (use geometry/location of this patch in Okd)

*Note:* In reality `Okd(x_1.0)` is `(Okd(x_is+x_1.0)-Okd(x_is))/1.0` with `x_is`
or initial slip as `0.0`

Vector *m* contains model parameters (slip on each patch) being estimated, i.e.,

```
P1_slip_x
P1_slip_y
P2_slip_x
P2_slip_y
...
```

Vector *d* contains the observation data, i.e.,

```
S1_Ux
S1_Uy
S2_Ux
S2_Uy
...
```

Usage: See `README`
