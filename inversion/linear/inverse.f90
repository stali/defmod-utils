program main

  use driver

  integer :: status, i, j, ngps, npatches
  real(8), allocatable :: x(:), y(:), ugps(:,:), geom(:,:), G(:,:), d(:),      &
     sol(:), M(:,:)
  real(8) :: skip, u(3), uu(9), nu, mu, z
  character(32) :: arg1, arg2

  nu=0.25
  mu=3.0E10 
  z=0.0

  call getarg(1,arg1)
  call getarg(2,arg2)

  ! Read GPS data
  open(10,file=arg1)
  read(10,*) ngps
  allocate(x(ngps),y(ngps),ugps(ngps,2))
  do i=1,ngps
     read(10,*) x(i),y(i),ugps(i,1),ugps(i,2)
  end do
  close(10)
  ! Read fault patch info (Number of patches and their geometry)
  open(10,file=arg2)
  read(10,*) npatches
  allocate(geom(npatches,7))
  do i=1,npatches
     read(10,*) geom(i,1:7)
  end do
  close(10)
  ! Form G Matrix
  allocate(G(2*ngps,2*npatches))
  do i=1,ngps
     do j=1,npatches
        call okada3d(geom(j,1),geom(j,2),geom(j,3),geom(j,4),geom(j,5),        &
           geom(j,6),geom(j,7),(/1.0d0,0.0d0,0.0d0/),x(i),y(i),z,nu,mu,u,uu)
        G(2*i-1,2*j-1)=u(1)
        G(2*i,2*j-1)=u(2)
        call okada3d(geom(j,1),geom(j,2),geom(j,3),geom(j,4),geom(j,5),        &
           geom(j,6),geom(j,7),(/0.0d0,1.0d0,0.0d0/),x(i),y(i),z,nu,mu,u,uu)
        G(2*i-1,2*j)=u(1)
        G(2*i,2*j)=u(2)
     end do
  end do
  allocate(sol(npatches*2),d(ngps*2))
  ! Form RHS
  do i=1,ngps
     d(2*i-1)=ugps(i,1)
     d(2*i)=ugps(i,2)
  end do
  ! Solve the Linear System
  allocate(M(2*npatches,2*npatches))
  M=matmul(transpose(G),G)
  call MatInv(M)
  sol=matmul(M,matmul(transpose(G),d))
  ! Print Slip Values
  do i=1,npatches
     print'(A6,I2,X,A7,2F10.3)', "Patch", i, "slip = ", real(sol(2*i-1)),      &
        real(sol(2*i))
  end do

contains

  ! Computes Inverse of a Matrix
  subroutine MatInv(Mat)
    implicit none
    real(8) :: Mat(:,:)
    real(8), allocatable :: work(:)
    integer :: n, lwork, info
    integer, allocatable :: ipiv(:)
    n=size(Mat,1)
    allocate(ipiv(n),work(1))
    lwork=-1
    call dgetrf(n, n, Mat, n, ipiv, info)
    call dgetri(n, Mat, n, ipiv, work, lwork, info)
    lwork=int(abs(work(1)))
    deallocate(work)
    allocate(work(lwork))
    call dgetri(n, Mat, n, ipiv, work, lwork, info)
    deallocate(ipiv,work)
  end subroutine MatInv

end program main
