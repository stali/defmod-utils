program main

  use driver

  implicit none
  real(8) :: x, y, u(3), uu(9), disp(3)
  integer :: i, j, k
  uu = 0.0
  
  k = 0
  do i = -25000, 25000, 2500
     do j = -25000, 25000, 2500
        k=k+1
     end do
  end do
  print*, k

  do i = -25000, 25000, 2500
     do j = -25000, 25000, 2500
        x = real(i)
        y = real(j)
        disp=0.0
        ! Patch 1
        call okada3d(5000.0d0, 10000.0d0, 0.0d0, 90.0d0, 90.0d0,-5000.0d0,     &
           0.0d0, (/ 5.0d0,0.0d0,0.0d0/), x, y, 0.0d0, 0.25d0, 30000000000.0d0,&
           u, uu)
        disp=disp+u
        ! Patch 2
        call okada3d(5000.0d0, 10000.0d0, 0.0d0, 90.0d0, 90.0d0,    0.0d0,     &
           0.0d0, (/10.0d0,0.0d0,0.0d0/), x, y, 0.0d0, 0.25d0, 30000000000.0d0,&
           u, uu)
        disp=disp+u
        ! Patch 3
        call okada3d(5000.0d0, 10000.0d0, 0.0d0, 90.0d0, 90.0d0, 5000.0d0,     &
           0.0d0, (/ 5.0d0,0.0d0,0.0d0/), x, y, 0.0d0, 0.25d0, 30000000000.0d0,&
           u, uu)
        disp=disp+u
        print'(2F15.3,3F12.6)', x, y, disp
     end do
  end do

end program main
