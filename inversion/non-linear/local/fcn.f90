! Objective function being minimized

subroutine fcn(np,p,h)

  use svars
! use model

  implicit none
  integer :: np, i, n
  real(8) :: p(np), h
  real(8), allocatable :: phamod(:), phadev(:)
! real(8) :: nu, wl, h_exact, h_approx
! real(8), allocatable :: xu(:,:), u(:,:), xlocdx(:,:)

  n=size(phaobs)
  allocate(phamod(n), phadev(n))
  phamod=phamod0+matmul(G1,p-x0)
  if (switch==-1) then
     do i=1,n
        print*, xloc(i,:),phamod(i)
     end do
  end if
  h=sum(abs(phaobs-phamod))/n

! h_approx=h
! wl=0.0568 ! ERS
! allocate(xu(n,3),u(n,3))
! nu=p(15)
! call gipht_okada(p(1:10), xloc, nu, xu)
! u=xu
! call gipht_mogi(p(11:14), xloc, nu, xu)
! u=u+xu
! do i=1,n
!    phamod(i)=-4.0*pi*dot_product(u(i,:),unitv(i,:))/wl
! end do
! h_exact=sum(abs(phaobs-phamod))/n

end subroutine fcn
