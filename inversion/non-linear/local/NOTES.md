Instead  of  solving the exact forward problem during optimization, we solve the
approximate problem  using  its *Taylor series* approximation.  This approach is
useful when the forward calculation is expensive.

File `params.txt` contains parameters which we want to optimize.

File `data.txt` contains  observation  data (InSAR range change) generated using
parameters in `params_orig.txt` which we want to recover.

Usage: See `README`

Note: Script `genG.sh` calls the forward modeling program `fmdl` to generate the
partial derivatives (evaluated  using finite difference formulas, using the step
size given in `params.txt`) used in the *Taylor series* approximation.
