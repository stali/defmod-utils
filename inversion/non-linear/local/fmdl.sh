#!/bin/bash
if [ ! -f fmdl ]; then
   gfortran -Wall -o fmdl ../global/m_model.f90 fmdl.f90
   rm -f *.mod
fi
./fmdl $1 $2
