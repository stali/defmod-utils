program main

  use model

  implicit none
  integer :: np, ndata, i
  real(8) :: skip, nu, wl
  real(8), allocatable :: p(:), xloc(:,:), phamod(:), unitv(:,:), xu(:,:),     &
     u(:,:)
  character(32) :: arg1, arg2
  character(16) :: skipc

  wl=0.0568 ! ERS

  ! Read parameters
  call getarg(1,arg1)
  open(10,file=arg1)
  read(10,*) np
  allocate(p(np))
  do i=1,np
     read(10,*) skipc, p(i), skip, skip, skip
  end do
  close(10)

  ! Read coordinates and unitv
  call getarg(2,arg2)
  open(10,file=arg2)
  read(10,*) ndata
  allocate(xloc(ndata,2),unitv(ndata,3))
  do i=1,ndata
     read(10,*) xloc(i,:), skip, unitv(i,:)
  end do
  close(10)

  allocate(xu(ndata,3),u(ndata,3),phamod(ndata))
  nu=p(15)
  call gipht_okada(p(1:10), xloc, nu, xu)
  u=xu
  call gipht_mogi(p(11:14), xloc, nu, xu)
  u=u+xu
  do i=1,ndata
     phamod(i)=-4.0*pi*dot_product(u(i,:),unitv(i,:))/wl
  end do
  do i=1,ndata
     print*, xloc(i,:),phamod(i)
  end do

end program main
