#!/bin/bash

# Generates partials for use in Taylor approximation

# Usage: bash genG.sh params.txt

rm -f fx G1 
np=`awk 'NR==1{print $1}' $1`
./fmdl.sh $1 data.txt | awk '{print $3}' > fx
cat fx | awk '{print 0}' > zero
for i in `seq 1 $np`
do
  wl=$(($i+1)) 
  switch=`cat $1 | awk '{if ((NR=='$wl') && ($3==$4)) print "off"}'`
  if [ "$switch" == "off" ]; then
  cat zero >> G1
  else
  cat $1 | awk '{if (NR=='$wl') print $1,$2+$5,$3,$4,$5; else print $1,$2,$3,$4,$5}' > params.tmp
  ./fmdl.sh params.tmp data.txt | awk '{print $3}' > fxplush
  h=`cat $1 | awk '{if (NR=='$wl') print $5}'`
  paste fxplush fx | awk '{print ($1-$2)/('$h')}' >> G1
  fi
done
rm -f zero params.tmp fxplush
