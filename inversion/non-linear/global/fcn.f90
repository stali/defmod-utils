! Objective function being minimized (Modify as needed)

subroutine fcn(np,p,h)

  use svars
  use model

  implicit none
  integer :: np, i, n
  real(8) :: wl, p(np), nu, h, op(10), mp(4)
  real(8), allocatable :: xu(:,:), phamod(:), phadev(:), u(:,:)
  wl=0.0568 ! ERS
  n=size(phaobs)
  allocate(xu(n,3), phamod(n), phadev(n), u(n,3))
  nu=p(15)
  op=p(1:10)
  call gipht_okada(op, xloc, nu, xu)
  u=xu
  mp=p(11:14)
  call gipht_mogi(mp, xloc, nu, xu)
  u=u+xu
  do i=1,n
     phamod(i)=-4.0*pi*dot_product(u(i,:),unitv(i,:))/wl
  end do
  if (switch==-1) then
     do i=1,n
        print*, xloc(i,:),phamod(i)
     end do
  end if
  h=sum(abs(phaobs-phamod))/n

end subroutine fcn
