! Okada/Mogi routines to calculate surface deformation in GIPhT's convention,
! i.e., 10 parameters for Okada and 4 for Mogi
! 
! Please cite:
!   
!   - Okada Y, Surface deformation due to shear and tensile faults in a
!     half-space, BSSA, 75, p1135-1154, 1985
!
!   - Mogi K, Relations between the eruptions of various volcanoes and the 
!     deformations of the ground surfaces around them, 1958

module model

  real(8) :: pi=3.14159265359, deg2rad=0.01745329252, pi2inv=0.15915494309

contains

  ! Mogi
  subroutine gipht_mogi(p, xloc, nu, u)
    implicit none
    real(8) :: p(:), xloc(:,:), nu, u(:,:)
    integer :: n, i
    real(8) :: xe, xn, r3
    n = size(xloc(:,1))
    if (p(4) /= 0.0d0) then
       do i = 1, n
          xe = p(1) - xloc(i,1)
          xn = p(2) - xloc(i,2)
          r3 = ((nu - 1.0d0)*p(4)/pi) * (sqrt(p(3)**2 + xe**2+xn**2))**(-3.0d0)
          u(i,1) = xe*r3
          u(i,2) = xn*r3
          u(i,3) = -1.0d0*p(3)*r3
       end do
    else
       u = 0.0d0
    end if
  end subroutine gipht_mogi

  ! Wrapper to 'okada' below
  subroutine gipht_okada(p, xloc, nu, u)
    implicit none
    real(8) :: p(:), xloc(:,:), nu, u(:,:)
    real(8) :: length, width, depth, dip, strike, east, north, disl(3), x, y,  &
       disp(3)
    integer :: i, n
    length = p(1)
    width  = p(2)
    depth  = p(3)
    dip    = p(4)
    strike = p(5)
    east   = p(6)
    north  = p(7)
    disl   = p(8:10)
    n = size(xloc(:,1))
    if (sum(disl**2) /= 0.0d0) then
       do i = 1, n
          x = xloc(i,1)
          y = xloc(i,2)
          call okada(length, width, depth, dip, strike, east, north, disl, x,  &
             y, nu, disp)
          u(i,1) = disp(1)
          u(i,2) = disp(2)
          u(i,3) = disp(3)
       end do
    else
       u = 0.0d0
    end if
  end subroutine gipht_okada

  ! Okada
  subroutine okada(length, width, depth, dip, strike, east, north, disl, x, y, &
     nu, u)  
    implicit none
    real(8) :: nu, x, y, length, width, depth, dip, strike, east, north,       &
       disl(3), u(3)
    real(8) :: alp, angle, xl, yl, u1, u2
    alp = 1.0d0-2.0d0*nu
    angle = (strike-90.0d0)*deg2rad
    xl = cos(angle)*(x-east) - sin(angle)*(y-north) + 0.5d0*length
    yl = sin(angle)*(x-east) + cos(angle)*(y-north)
    call srectf(alp,xl,yl,depth,dip,length,0.0d0,width,0.0d0,disl(1),disl(2),  &
       disl(3),u(1),u(2),u(3))
    u1 = u(1)
    u2 = u(2)
    u(1)= cos(angle)*u1 + sin(angle)*u2
    u(2)=-sin(angle)*u1 + cos(angle)*u2
  end subroutine okada

  ! Okada's original routine (calls subroutine 'srectg') 
  ! with strain calculations removed
  subroutine srectf(alp,x,y,depth,dip,al1,al2,aw1,aw2,disl1,disl2,disl3,u1,u2, &
     u3)
    implicit real(8) (a-h,o-z) 
    dimension u(3),du(3) 
    data f0,f1/0.0d0,1.0d0/ 
    sd=sin(dip*deg2rad)                                            
    cd=cos(dip*deg2rad)                                             
    if(abs(cd) .lt. 1.0e-6) then                                       
       cd=0.0d0                                        
       if(sd.gt.0.) sd= 1.0d0                                          
       if(sd.lt.0.) sd=-1.0d0                                           
    endif
    p=y*cd+depth*sd 
    q=y*sd-depth*cd 
    do i=1,3
       u(i)=f0 
    end do
    do k=1,2 
       if(k.eq.1)  et=p-aw1
       if(k.eq.2)  et=p-aw2
       do j=1,2 
          if(j.eq.1)  xi=x-al1
          if(j.eq.2)  xi=x-al2 
          jk=j+k 
          if(jk.ne.3)  sign= f1 
          if(jk.eq.3)  sign=-f1 
          call srectg(alp,xi,et,q,sd,cd,disl1,disl2,disl3,du(1),du(2),du(3)) 
          do i=1,3 
             u(i)=u(i)+sign*du(i) 
          end do
       end do
    end do
    u1=u(1)
    u2=u(2)
    u3=u(3)
    return 
  end subroutine srectf

  ! Okada's original routine (called by subroutine 'srectf')
  ! with strain calculations removed
  subroutine srectg(alp,xi,et,q,sd,cd,disl1,disl2,disl3,u1,u2,u3)
    implicit real(8) (a-h,o-z)                                                         
    data f0,f1,f2/0.0d0,1.0d0,2.0d0/ 
    xi2=xi*xi 
    et2=et*et 
    q2=q*q 
    r2=xi2+et2+q2 
    r =sqrt(r2) 
    d =et*sd-q*cd 
    y =et*cd+q*sd 
    ret=r+et 
    if(ret.lt.f0)  ret=f0 
    rd =r+d 
    if( q .ne.f0)  tt = atan( xi*et/(q*r) ) 
    if( q .eq.f0)  tt = f0 
    if(ret.ne.f0)  re = f1/ret 
    if(ret.eq.f0)  re = f0 
    if(ret.ne.f0)  dle= log(ret) 
    if(ret.eq.f0)  dle=-log(r-et) 
    rrx=f1/(r*(r+xi)) 
    rre=re/r 
    if(cd.eq.f0) go to 20 
    ! Inclined fault                                       
    td=sd/cd 
    x =sqrt(xi2+q2) 
    if(xi.eq.f0) a5=f0 
    if(xi.ne.f0) a5= alp*f2/cd*atan((et*(x+q*cd)+x*(r+x)*sd)/(xi*(r+x)*cd))   
    a4= alp/cd*(log(rd)-sd*dle) 
    a3= alp*(y/rd/cd-dle)+td*a4 
    a1=-alp/cd*xi/rd-td*a5 
    go to 30 
    ! Vertical fault                                        
20  rd2=rd*rd 
    a1=-alp/f2*xi*q/rd2 
    a3= alp/f2*(et/rd+y*q/rd2-dle) 
    a4=-alp*q/rd 
    a5=-alp*xi*sd/rd 
30  a2=-alp*dle-a3 
    u1=f0 
    u2=f0 
    u3=f0 
    ! Strike slip contribution                                
    if(disl1.eq.f0) go to 200 
    un=disl1*pi2inv 
    req=rre*q 
    u1=u1-un*(req*xi+tt+a1*sd) 
    u2=u2-un*(req*y+q*cd*re+a2*sd) 
    u3=u3-un*(req*d+q*sd*re+a4*sd) 
    ! Dip slip contribution                                    
200 if(disl2.eq.f0) go to 300 
    un=disl2*pi2inv
    sdcd=sd*cd 
    u1=u1-un*(q/r-a3*sdcd) 
    u2=u2-un*(y*q*rrx+cd*tt-a1*sdcd) 
    u3=u3-un*(d*q*rrx+sd*tt-a5*sdcd) 
    ! Tensile fault contribution                               
300 if(disl3.eq.f0) go to 900 
    un=disl3*pi2inv 
    sdsd=sd*sd 
    u1=u1+un*(q2*rre-a3*sdsd) 
    u2=u2+un*(-d*q*rrx-sd*(xi*q*rre-tt)-a1*sdsd) 
    u3=u3+un*(y*q*rrx+cd*(xi*q*rre-tt)-a5*sdsd) 
900 return 
  end subroutine srectg

end module model
