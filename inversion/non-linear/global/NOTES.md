File `params.txt` contains parameters which we want to optimize.

File `data.txt` contains  observation  data (InSAR range change) generated using
parameters in `params_orig.txt` which we want to recover.

Usage: See `README`
