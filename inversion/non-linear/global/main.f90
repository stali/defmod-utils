! Shared variables (also used by fcn.f90) are defined here
module svars
  integer :: switch
  real(8), allocatable :: xloc(:,:), phaobs(:), unitv(:,:)
end module svars

! Main program
program main

  use svars
  use opt
  implicit none
  integer :: n, ndata, i, ierr, rank, nprocs
  integer, allocatable :: seed(:)
  real(8), allocatable :: x(:), lb(:), ub(:), skip(:), xopt(:), c(:), vm(:),   &
     xold(:)
  real(8) :: t, eps, rt, fopt
  integer :: ns, nt, neps, maxevl, nfcnev, nacc, nobds, stat
  logical :: maxm
  character(32) :: arg1, arg2, arg3; character(16), allocatable :: pname(:)
  external :: fcn

  call MPI_Init(ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nprocs, ierr)

  call random_seed(size=n)
  allocate(seed(n))
  seed = 1+rank**2
  call random_seed(put=seed)

  ! Read and distribute data
  call getarg(1,arg1)
  call read_params()
  call getarg(2,arg2)
  call read_obs_data()

  ! Set annealing parameters
  stat =-1
  maxm =.false.
  t    = 2.5
  rt   = 0.25
  vm   = 1.0
  eps  = 1.0E-2
  neps = 3
  nt   = 20
  ns   = 20
  c    = 2.0
  maxevl = 5000000

  ! Call the annealing routine
  call getarg(3,arg3)
  read(arg3,*) switch
  if (switch==0 .and. rank==0) then
     call fcn(n, x, fopt); print*, fopt
  else if (switch==1) then
     call sa(fcn, n, x, maxm, rt, eps, ns, nt, neps, maxevl, lb, ub, c, t, vm, &
        xopt, fopt, nacc, nfcnev, nobds, stat)
  else if (switch==-1 .and. rank==0) then
     call fcn(n, x, fopt)
  end if

  ! Print stats
  if (stat == 0 .and. rank == 0) then
     write(*,1000) fopt, nfcnev, nacc, nobds, nfcnev/nprocs, t
1000 format(' Optimal function value:   ',       g20.10,                       &
           /' Number of function evaluations:     ',i10,                       &
           /' Number of accepted evaluations:     ',i10,                       &
           /' Number of out of bound evaluations: ',i10,                       &
           /' Evaluations per CPU:                ',i10,                       &
           /' Final temperature:        ',       g20.10)
     print*, " "
     print'(1X,I0)', n
     do i=1,n
        print'(A17,4F15.3)', pname(i), xopt(i), lb(i), ub(i), skip(i)
     end do
  end if

  call MPI_Finalize(ierr)

contains

  subroutine read_params()
    ! Distribute input parameters and their bounds
    if (rank==0) then
       open(10,file=arg1)
       read(10,*) n
       allocate(pname(n),xold(n))
    end if
    call MPI_Bcast(n, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
    allocate(x(n),lb(n),ub(n),skip(n),xopt(n),c(n),vm(n))
    if (rank==0) then
       do i=1,n
          read(10,*) pname(i), x(i), lb(i), ub(i), skip(i)
       end do
       close(10)
       xold=x
    end if
    call MPI_Bcast(x,  n, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_Bcast(lb, n, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_Bcast(ub, n, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
  end subroutine read_params

  subroutine read_obs_data()
    ! Distribute observation data and look angle needed by 'fcn'
    if (rank==0) then
       open(10,file=arg2)
       read(10,*) ndata
    end if
    call MPI_Bcast(ndata, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
    allocate(xloc(ndata,2),phaobs(ndata),unitv(ndata,3))
    if (rank==0) then
       do i=1,ndata
          read(10,*) xloc(i,:), phaobs(i), unitv(i,:)
       end do
       close(10)
    end if
    call MPI_Bcast(xloc, 2*ndata, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_Bcast(phaobs, ndata, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_Bcast(unitv,3*ndata, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
  end subroutine read_obs_data

end program main
